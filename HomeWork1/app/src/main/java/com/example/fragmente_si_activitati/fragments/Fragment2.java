package com.example.fragmente_si_activitati.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.fragmente_si_activitati.R;
import com.example.fragmente_si_activitati.activity.MainActivity2;
import com.example.fragmente_si_activitati.interfaces.ActivityFragmentCommunication;

public class Fragment2 extends Fragment {

    private ActivityFragmentCommunication activityFragmentCommunication;

    public static Fragment2 newInstance() {

        Bundle args = new Bundle();

        Fragment2 fragment = new Fragment2();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_2,container,false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button btn = view.findViewById(R.id.btn1_fragment2);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //openMainActivity2();
                if (activityFragmentCommunication != null) {
                    activityFragmentCommunication.replaceWithFragment3();
                }


            }
        });

        Button btn2 = view.findViewById(R.id.btn2_fragment2);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //openMainActivity2();
                if (activityFragmentCommunication != null) {
                    activityFragmentCommunication.goBackToFragment1();
                }

            }
        });

        Button btn3 = view.findViewById(R.id.btn3_fragment2);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //openMainActivity2();
                if (activityFragmentCommunication != null) {
                    activityFragmentCommunication.closeActivity();
                }

            }
        });


    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof ActivityFragmentCommunication) {
            activityFragmentCommunication = (ActivityFragmentCommunication) context;
        }
    }
}
