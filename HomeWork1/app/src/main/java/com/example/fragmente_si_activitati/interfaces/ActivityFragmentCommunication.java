package com.example.fragmente_si_activitati.interfaces;

public interface ActivityFragmentCommunication {

   void openMainActivity2();
   void addFirstFragment();
   void addSecondFragment();
   void replaceWithFragment3();
   void closeActivity();
   void goBackToFragment1();
}
