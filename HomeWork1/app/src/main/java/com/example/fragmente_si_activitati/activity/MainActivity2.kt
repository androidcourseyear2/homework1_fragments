package com.example.fragmente_si_activitati.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.example.fragmente_si_activitati.R
import com.example.fragmente_si_activitati.fragments.Fragment1
import com.example.fragmente_si_activitati.fragments.Fragment1A2
import com.example.fragmente_si_activitati.fragments.Fragment2
import com.example.fragmente_si_activitati.fragments.Fragment3
import com.example.fragmente_si_activitati.interfaces.ActivityFragmentCommunication

class MainActivity2 : AppCompatActivity() , ActivityFragmentCommunication{
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        addFirstFragment()
        //addSecondFragment()

    }



    override fun openMainActivity2() {
        TODO("Not yet implemented")
    }

    override fun goBackToFragment1() {

        val fragmentManager=supportFragmentManager
        fragmentManager.popBackStackImmediate()

    }

    override fun closeActivity() {
        this.finish()
    }


    override fun addSecondFragment(){
        val fragmentManager=supportFragmentManager
        val transaction=fragmentManager.beginTransaction()
        val tag= Fragment2::class.java.name
        val addTransaction=transaction.add(
            R.id.frame_layout,Fragment2.newInstance(),tag
        )

        addTransaction.addToBackStack(tag)
        addTransaction.commit()


    }

    override fun replaceWithFragment3(){
        val fragmentManager=supportFragmentManager
        val transaction=fragmentManager.beginTransaction()
        val tag= Fragment3::class.java.name
        val replaceTransaction=transaction.replace(
            R.id.frame_layout,Fragment3.newInstance(),tag
        )

        replaceTransaction.commit()
    }

    override fun addFirstFragment() {

            val fragmentManager=supportFragmentManager
            val transaction=fragmentManager.beginTransaction()
            val tag= Fragment1A2::class.java.name
            val addTransaction=transaction.add(
                R.id.frame_layout,Fragment1A2.newInstance(),tag
            )

            addTransaction.addToBackStack(tag)
            addTransaction.commit()

    }

    override fun onBackPressed() {
        finishAffinity()
    }

}